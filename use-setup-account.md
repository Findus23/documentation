# Setup your account

## Create an account on PeerTube

To be able to upload a video, you must have an account on an _instance_ (a server running PeerTube). The PeerTube project maintains a list of
public instances at [instances.joinpeertube.org](https://instances.joinpeertube.org/instances) (note that instance administrators must add themselves
to this list manually, it is not automatically generated).

Once you've found an instance that suits you, click the button "Create an account" and fill in you **login** (nickname), a **email address** and a **password**.

![Registration page after clicking on "Create an account"](/assets/profile_registration.png)

## Connect to your PeerTube instance

To log in, you must go to the address of the particular instance that you registered on. Instances share each other's videos, but
each instance's index of accounts is not federated. Click on the "Login" button in the top-left corner, then fill in your login/email address and 
password. Note that the login name and password are **case-sensitive**!

Once logged in, your nickname and mail address will appear below the instance name.

## Update your profile

To update your user profile, change your avatar, change your password, etc. click on the three vertical dots near your profile name to pop
up a menu, then on the "My account" item. You then have a few options:

1.  My settings
1.  My notifications
1.  My library
1.  Misc (for managing mutes and ownership)

### My Settings

In the **_My Settings_** tab settings are broken up into several sections.

Before any of the other sections, you can:

* Change your Avatar. The allowed filetypes and file-size should be displayed near the "Change the Avatar" button.
* View your Upload Quota and used space. Your upload quota is determined by the instance that hosts your account. If your instance creates mulitple versions of your videos in different qualities then all versions count towards your quota.

#### Profile

* Modify your Display Name. Note that your Display Name is not the same as your Username (used for logging in), which cannot be modified.
* Add or modify your User Description. This will be displayed on your Public Profile in the "About" section.

#### Video Settings

* Change how videos with sensitive content are displayed. You can choose to list them normally, blur their content, or hide them completely. This feature may be disabled on your instance.
* Select which languages you want to see videos in. Languages which are not selected will be hidden. You can have up to 20 languages (or all languages) selected.
* Change whether videos autoplay or not.
* Change whether you use WebTorrent or not.

#### Notifications

* Change which notifications you receive. You can choose to receive notifications in PeerTube, via Email, both, or neither.

#### Interface

* Change your Theme. In earlier versions (<=v1.3.1) the theme was selected in the sidebar.

#### Password

* Change your Password. To change your password you will need to enter your current password, and then enter your new password twice before clicking **_Change Password_**.

#### Email

* Change your Email. You will need to enter your password again to change this setting.

#### Danger Zone

* Delete your account. This is irreversible.